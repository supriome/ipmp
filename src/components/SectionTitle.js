import React from 'react'
import '../styles/SectionTitle.sass'

const SectionTitle = (props) => {
  return (
    <div className="sectitle">
      <div className="sectitle__content">
        {props.children}
      </div>
    </div>
  )
}

export default SectionTitle
