import React, { Component } from 'react';
import { Link } from 'gatsby';
import { v4 } from 'uuid'
import _ from "lodash"
import BackgroundImage from 'gatsby-background-image';
import '../styles/ArticleThumbnail.sass';
import '../styles/search.sass';

// Search component
export default class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			query: ``,
			results: [],
		};
	}

	render() {

		const flattenArticle = (arr) =>
			arr.map(({ node: { ...rest } }) => ({
				...rest,
			}));

		const article = flattenArticle(this.props.posts);

		const filterValue = (query) => {
			
			const result = _.filter(article, function(o) {
				if (!!query) {
					return (
						_.includes(o.frontmatter.title, query) ||
						_.includes(o.excerpt, query) ||
						_.includes(o.rawMarkdownBody, query)
					);
				} else {
					return '';
				}
			});

			return result;
		};

		const search = (evt) => {
			const query = evt.target.value;
			this.setState({
				query,
				results: filterValue(query),
			});
		};

		

		return (
			<div className="search-container">
				<div className="search-box-container">
					<input
						className="search-box"
						placeholder="文章搜索"
						autoFocus
						type="text"
						value={this.state.query}
						onChange={search}
					/>
				</div>
				<div className="columns" style={{ flexWrap: 'wrap' }}>
					{this.state.results.map((item) => {
						return (
							<div className="post-card column is-6" key={v4()}>
								<Link
									to={item.fields.slug}
									style={{ display: 'block', height: '100%' }}
								>
									<article className="content pointer articlethumbnail">
										<div>
											{item.frontmatter.featuredimage ? (
												<BackgroundImage
													className="roll-cover"
													fluid={
														item.frontmatter.featuredimage.childImageSharp.fluid
													}
												/>
											) : (
												<div className="roll-cover bg-default"></div>
											)}
											<h2 className="title">{item.frontmatter.title}</h2>
										</div>
										<div>
											<p className="excerp">{item.excerpt}</p>
											<div className="meta">{item.frontmatter.date}</div>
										</div>
									</article>
								</Link>
							</div>
						);
					})}
				</div>
			</div>
		);

		
	}
}
