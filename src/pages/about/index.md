---
templateKey: about-page
path: /about
title: 关于 NGOCN
---
NGOCN是一家中国独立媒体，非营利性质，致力向公众提供进步、负责任且多元的纪实性内容，目前由认同其理念志愿者运营。

## 使命

独立发声，促进社会变革。