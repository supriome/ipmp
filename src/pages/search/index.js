import React from 'react';
import { Helmet } from 'react-helmet';
import { graphql } from 'gatsby';
import Layout from '../../components/Layout';
import Search from '../../components/search';

const searchPage = ({data}) => {

    return (
			<Layout>
				<section className="section">
					<Helmet title={`Search`} />
					<div className="container content">
						<div className="columns">
							<div
								className="column is-10 is-offset-1"
								style={{ marginBottom: '6rem' }}
							>
								<Search posts={data.allMarkdownRemark.edges} />
							</div>
						</div>
					</div>
				</section>
			</Layout>
		);
};

export default searchPage;

export const searchPageQuery = graphql`
    query SearchIndexQuery {
        allMarkdownRemark(
            sort: { fields: [frontmatter___date], order: DESC }
            filter: { frontmatter: { templateKey: { eq: "article" } } }
            limit: 1000
        ) {
            totalCount
            edges {
                node {
                    excerpt(pruneLength: 60, truncate: true)
                    id
                    fields {
                        slug
                    }
                    frontmatter {
                        title
                        templateKey
                        date(formatString: "YYYY/MM/DD")
                        columnist {
                            fields {
                                slug
                            }
                            meta: frontmatter {
                                title: columnist_title
                            }
                        }
                        featuredpost
                        featuredimage {
                            childImageSharp {
                                fluid(maxWidth: 400, quality: 80) {
                                    ...GatsbyImageSharpFluid
                                }
                            }
                        }
                        description
                    }
                    rawMarkdownBody
                }
            }
        }
    }
`;
