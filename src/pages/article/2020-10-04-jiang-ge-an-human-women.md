---
templateKey: article
title: 江歌案：“人性”在带节奏，对女性的暴力却无人看见
slug: jiang-ge-an-human-women
date: 2017-11-14T08:17:00.000Z
description: 江歌案，这起发生在一年前的日本留学生凶杀案，因为遇难学生江歌母亲和其生前好友、也是案件重要证人刘鑫的见面视频，而成为网络舆论热点。
featuredpost: false
featuredimage: /img/j1.webp
trending: 2
isbrief: false
tags:
  - 江歌案
  - 网络暴力
  - 女性
  - 咪蒙
---
江歌案，这起发生在一年前的日本留学生凶杀案，因为遇难学生江歌母亲和其生前好友、也是案件重要证人刘鑫的见面视频，而成为网络舆论热点。

![](/img/j1.webp)

事件回顾：江歌是一名留学日本的女生，在2016年11月3日被害于其位于东京的公寓门口，据资料显示，江歌遇害时其室友刘鑫也在屋内，后日本警方逮捕的犯罪嫌疑人陈世峰正是刘鑫的前男友。

江歌母亲表示，案发不久后，刘鑫和家人便失联了，为了让刘鑫作供，江歌母亲以《泣血的呐喊：刘鑫，江歌的冤魂喊你出来作证！》为标题，在网络上公布了刘鑫及家人的个人信息。此外，江歌母亲在网络上征集签名，想在日本请愿：判决陈世峰死刑，目前已获得14万网友签名。该案将在今年12月11日在东京审理。

现时，舆论的重点已经转移到刘鑫身上，由于疑凶是她的前男友，而且在案发前江歌曾告诉母亲，疑凶曾到公寓找刘鑫并有过口角，因此不少观点认为，江歌是为了帮刘鑫从前男友中解围，才让刘鑫先回到公寓内，她则独自面对疑凶最终被杀害，而刘鑫在案发后对江歌母亲的态度，以及案发时没有积极施救，都被网民评为“没人性”、“渣友”。

![](/img/j2.webp)

*咪蒙公众号以“制裁人性”为标题，成为网络热文*

**但这起案件中，被舆论忽视的性别暴力问题，更值得我们关注。**

网络上有自称曾在大学期间，与本案疑凶陈世峰短暂交往的女生就此发文，称在想与陈世峰分手时，两人发生冲突，该女生在扇了陈世峰一巴掌后，被其拖到角落踹肚子和掌掴。但这件事被学校盖下去了，当事人想报警的诉求也没有实现，只得到老师强调陈世峰是好学生的回复。

![](/img/j3.webp)

*自称是疑凶前女友的网友文截图*

如果该文内容属实，那么陈世峰一直都有分手暴力的行为，但因为他是个好学生，这种行为在老师眼中变成了一个“小问题”，不仅没有追究责任，还帮忙“救火”。微博@我是落生在转发时评论道：“在女性遭遇暴力的时候，有多少手握权力及有话语权的人们，选择了和陈世峰当时的学校一样的处理方式呢？”

现实中，这种分手暴力甚至杀人的案例并非少数，其中大部分是针对女性的暴力行为。台湾现代妇女基金会曾在2008年的报告中指出，台湾每月平均有14.7件恋爱暴力、分手暴力和情杀的新闻事件报道，也就是每2天就发生一起恋爱暴力与杀人案件，平均每月至少有3件情人间的杀人未遂和杀人致死案件发生。

**以“分手+杀人”为关键词，也能搜索出不少案件**：

![](/img/j4.webp)

这些暴力行为并不会通过“道德声讨”得到解决。

**女权主义者、评论作者吕频在微博中写了一篇分析，NGOCN已获授权与读者分享：**

> 女生被害那事，母亲说什么做什么都没啥不对，其他众声汹汹的，其实还是责备受害者。责备受害者这种现象之所以总是看似有理，就是因为受害者都做得不好。其实就一没经验懦弱自保的常人，其人品，也能在大众中50%的水平上吧。通过责备这个人，这事完成了什么呢，从一个可以有各种社会检讨的性别暴力事件，转变成一个无解的极端道德拷问题了。*（注：评论说不觉得此人的道德水平在50%上。那就改成她远低于道德水平均线，只属于后10%的吧，所谓差劲的人，也行。）*
>
> 为啥是无解，围观者并不觉得，大家能列一堆在那个时刻能做得更好的选项，而且相信自己能做到。其实不是这样的。行为经济学的实验说，人在清醒的时候总是高估自己在情绪化时刻中的理智与道德。关键第一要避免自己和相关者陷入那种危险境地，第二要有对紧急状态的提前设想和应对预案——是性别暴力应对的知识和演练问题。
>
> 要没有这种准备，有人站出来见义勇为，当然是特别人性光辉，有人躲起来甩锅，也不足为奇。由此可见，就此事将焦点完全放在道德和人性之拷问，恰好反映出，大家放弃，忽视，承认了性别暴力的（不可）治理。
>
> 所以声浪再大，没法避免这些事情一次次再发生。——所以这些激烈正义观的带节奏，其实是危险的。
>
> 我不会一概排除集体道德拷问的正当性。只是就此事来说，简直没有人主张通过法律来裁决那个女生的责任。法律当然不是什么神圣万能，但是法律（理想状态下）能给惩罚也能给一个必要的界限。为何界限是必要的：如果一个人只要活着就赎不了她的罪，那只能说明她的罪名是被滥施的。
>
> 一个人为罪错所该负的责任一定要有局限，这是一个社会能维持的基础之一。而且她的道义上的责任不能是无可替换的。例如出钱和坐牢都是替换和了结道义拷问的方法。如果没有界限，没有了结选项，那这次万众道德轰炸简直就是为了满足参与者自己，因为并不想为事件要一个解决。
>
> 激情弥补无法还原的事实，行使判决的主权。简直没人相信法治了。相信的是权力。这个权力看似是被道德支撑的，然而其自信还是来自，大家知道可以一起惩罚那些“坏人”，毁坏她们的生活，让她们“恐惧”。当道德正义感需要靠能令人恐惧来自我赋权的时候，道德正义只是一层皮了吧。

*（在此补充豆瓣冷血才女的评论——这声讨反映了不安全感下的控制欲：控制住能控制的，欺负能欺负的。“越是无能为力，越希望能做些什么来推进“正义”的完成（比如整治一个在他们眼中罪不可恕的不道德者）。这种情绪和尝试都相当危险。”）*

> 当然责备庸众，也没啥意思。只是，今天的舆情已经和雷yang案时大不一样了，自那以后，集体愤怒相当成功（幸好还不是全部）地被引流到针对各种小角色，没有体制内权力的个人了，包括一堆坏女人，为老不尊的，熊孩子，插队的……当“贱民”相互撕咬，一个在上的父权更超然和自如。
>
> 人都觉得自己真诚有理。但是个人的真诚不能为一个没有人格的集体现象担保，而且真诚的个人在一个结构里又是怎么回事？很多时候这样的热血感就不愿意有觉察，所以就得有点觉察。另外，就这事泼冷水完全影响不了啥——这又是这些议论没有公共性的一个表现：没有议程和目标，和案件如何结果简直没关系。
>
> 还有求死刑，母亲可以求死刑，其实任何人都可以，这是言论自由。但是就一个没开庭，事实未经辩证的案件预设一个具体的判决标准，这也是不相信法治的表现，只能说是粗陋的同态复仇观。还有，如果你认为你所见的一些罪大恶极的人值得判死刑，那你就间接默许了聂树斌那样的冤案，因为死刑只要存在就会有错案。有时候坚持某种原则很难，但是至少我尽量不选择性地讲原则。我只能反对一切死刑，就算不去直接反对死刑，我也反对求死刑，这在我看来也是危险的道德暴力化。

![](/img/j5.webp)

*请求书的签名页面*

*补充：另，评论多说，大家的愤怒不是针对女生当时的不作为，而是针对她事后的表现。但问题还是：1/她事后说了几句什么话之类，这就案件来说是非关键因素，却被无限关注。2/“容错率极低”的道德高压对一个懦弱自私的人，这种巨大的不对称注定不可能取得什么对话结果，目前的情况就是证明。3/其实没有她能让这么多人满意和放过她的选项。*

*我同意有人说的，这是集体丢石头，这是一种私刑。*

### 留言回声机

昨天，我们也征集了读者对此事的看法

**小编选择了一部分与各位分享**

▶后真相时代，自媒体的情绪化报道，愤怒的受众，情理与法理的矛盾，江歌案过了之后还会有别的引爆点出现，为什么这样的事总是被消费化，在这个信息时代为什么大多数的人都是易感人群，都被带节奏。抛开江歌案这个极端挑战人道德底线的事，平庸之恶，在集体中以暴制暴，不用为自己的行为负责。这样的社会，会好吗？

▶emmmmmmm对于这事我说不出什么就是心寒

▶希望那个破男人被绳之以法。希望江歌妈妈能够走出悲伤，恢复正常生活。她那么长时间以来，都把女儿当成是唯一的生活支柱，现在她把所有理性和非理性的情绪都撒向刘鑫，不见得对她是好的，她需要一些其他的支持。但这些支持在法律给出一个判决之前又很难起作用。。。

▶在真相没有出来之前不想去一昧的去指责哪一方，看见很多知名的公众号因为看了局面的视频而激烈的指责刘鑫，或者说她逃避就是不对的就是应该面对……各种各种批评的言论倒向刘鑫的时候，我能理解群众的愤慨，但是，这不是正义。

我觉得世界上所有事情所有人性都是正常发生，没有什么好指责的，只是在危难面前不同的选择能让我们走到不同的地步不一样的境遇，如果多一份勇气多一份承担与责任感，会让整个世界多一些善意。

可是，若这些善意没有人在危机时第一时间想起，那这个事情就错了吗？江歌妈妈说，（大意）我需要刘鑫的道歉，因为她不道歉，就会影响这个社会的善恶观。是这样，是没错，善恶观因为频发的热点而时时刻刻在受冲击，之前的小悦悦、之前的老人摔倒没人扶、频发的碰瓷……江歌妈妈不愿承认现实，她相信只有这样，江歌还活着，看到这一幕我觉得很心酸，真的在遇到这样的打击，心里的窟窿是怎么都不能被弥补的，我希望能有心理医生去引导江歌妈妈，也希望坚强的江歌妈妈能有一个能为其避风雨的港湾。每个人都不美，但正是因为这样的残缺才构成这个异彩的世界。等待12月的开庭，我们等待真相。

▶从来不相信这个世界上有真正的混蛋，只是支离破碎的真相无法看到真心。尼采说过从来都没有什么道德现象只有对现象的道德解释。那么这件事除了站在道德高地批评之外就没有别的视角了吗？目前看到的真相都是支离破碎无法判断。但是对相关者的指责，庞大到令人恐惧与担忧。没有人试图去理解那个孩子，而是一味的指责。这样无助于所谓人性的回归。

▶这件事情给我的第一感觉是，以后我朋友发生什么事，我会不会无所顾忌地帮她。我现在都在想象江歌被害时，朝着房门求救时的绝望……面对危险我们潜意识可能都是逃避，但刘鑫的做法不仅是逃避更是人性中自私至极的体现，这种人真的好可怕。

▶刘鑫被众人趋之若鹜地声讨掩盖了对真凶的谴责。刘鑫本可以有警察和social worker协助她（这件事情对她来说也是有创伤的）。可是并没有相关的社会帮助，刘鑫也不知道这样的resource。所以她后期的没有任何逻辑的回答和做法使得人们对她的声讨成了social justice。只希望social justice可以制裁真正犯错的人，不管是法律还是良心。

▶从个人道德上没有一丝一毫认同刘鑫，但道德更多是用来自律的。微博上八成网友都赞同以舆论暴力的方式对待这件事，即使众人群起而攻之制裁她，对事件本身似乎没有很大的意义。众人忙着站队，忙着在这只“狗”落水的时候再踩上一脚。 “事实已经不能再清楚了还要问什么呢？” “你这么冷漠是不是和他一伙的？” 在无数的脚印上再踩上一脚的时候，众人究竟是为了捍卫心中的“正义”还是发泄自己的愤怒呢？ 我不知道，所以我选择不踏上自己的脚印。 只是当这只狗咬人的时候，我会站在前面。

▶网络暴力太可怕了。但其实不仅仅在这件事上，新浪微博上处处是网络暴力。这种暴力在于，以道德地名义去过度辱骂人，出言之粗俗、无逻辑、极端，难以让人认为是良善。但偏偏大部分人觉得自己更“善良”、更“正义”。在此次案件中，网民仍然不改本态。只是这次有了自媒体助兴，有了签名情愿，显得更加恐怖。我们看到的大概是一部分真相，刘到底是怎样一个人，面对好友为她而死该如何应对，恐怕许多人无法感同身受。也许她应该被谴责，但假设网络暴力成真，她是不是也被判“死刑”？关于杀人凶手陈也是一样，旁观者可以一起让一个人死，不得不说十分可怕。退到江母来说，这样的舆论和签名，其实是让她仇恨更多痛苦更深。此时不禁感叹，法律很重要，否则人们集体失控，太可怕。

▶感谢王局的跟进。不管案情判决结果如何，只要刘鑫余生都记得江歌死前的惨叫与模糊血肉就够了。

▶“唯有完人才够资格向罪人扔石头，但是，完人是没有的。”-吴经熊

▶我个人觉得这个事件很难用一两句话概括。第一，江歌妈妈的一些做法确实给刘鑫造成了困扰，甚至可能算违法行为。第二，刘鑫的做法确实也有不妥当的地方，但网友的谩骂……第三，我觉得杀人偿命是传统逻辑，但不是法律。法律面前，排序是:法理情。以所谓“民意”、舆论施加逼迫，反而会造成“群氓之恶”。以“正义”的名义作恶，仍然是恶。五四的时候，梁漱溟对大学生们说过一句话“我承认你们是爱国的，但你们做的事情是违法的。”我想对微博上的“声援者们”说，我承认你们是善良的，但你们基于此做出的行动、以及日后之影响很可能是“违法的”。我们口口声声说要把我们的国家建设成一个法治社会，可是关键时候没人相信法律，他们只相信“杀人偿命，天经地义”。这是可悲的。

▶1、遵守司法循法律规则和逻辑推理的形式正义。2、强烈的道德感是否会堕落成不道德。3、刘鑫的行为应该依法处理，作为社会的一个人其权利也受法律保护。4、如果是我当时的房间，我不确信自己不会像刘鑫一样。5、这个话题终将被其它话题代替，参与话题者重在娱乐参与还是参与建设，也包括我自己。

▶今天朋友圈被咪蒙的文章刷屏，看完之后觉得有些可怕。我个人本是不赞成用网络暴力制裁一个人这种方法的，看看完咪蒙的文章，我竟隐隐觉得她是对的，我们应该惩罚刘鑫，过一会后才察觉到自己情绪上的波动。我什么时候变得如此轻易被人煽动?我大概要好好反省自己了。这是对你说的，也是对我自己。晚安。

▶如果一个国家的法律被民意和舆论方向所控制，那么这个国家的法律也就完了。通过所谓网上签名去判决一个人执行死刑，民主和民粹，一线之差。参考典故“苏格拉底之死”和电影《十二怒汉》

▶关于“网络暴力”之类已经有许多后觉者说很清楚了，甚至隐隐讲篇。我比较关心且好奇的是江母“请愿书”。请愿书在港台，在外国似乎是有相关条约比如满多少多少人真的可以怎样怎样，那在中国大陆呢？应该怎么评价“请愿书”与舆论煽动的关系？