---
templateKey: article
title: 致读者：你愿为我续一秒，我就敢大声一点
slug: to-reader-xu-yi-miao
date: 2017-09-02T13:10:00.000Z
description: 今年五月，我们的微信进行了改版。新的slogan可谓十分应景：“世界在沉默，我们有话说”。
featuredpost: false
featuredimage: /img/xu1.jpg
trending: 1
isbrief: false
tags:
  - 发声
  - 理想主义
  - 纪录
---
![](/img/xu1.jpg)

今年五月，我们的微信进行了改版。新的slogan可谓十分应景：“**世界在沉默，我们有话说**”。

这两个多月来，我们的通知栏是这样的：

![](/img/xu2.webp)

圈起的通知均是被删文通知，最近一篇就在今早

![](/img/xu3.webp)

这还不包括小号的文章。

毫无疑问，我们正活在一个不断“消声”的网络环境中。“消声”可能是审查所致，也可能是市场导向。不管出于何种原因，有一部分具有公共价值、涉及公共利益的重要议题长期“不见天日”，有一群被社会边缘化的人长期“无声无息”。

但我们从没想过要改变初衷，既不会因“怕删”而保持沉默，也不会因“没流量”而放弃小众议题。当然，这个环境逼着人要更有智慧，我们尝试用邮件订阅、多平台联合的方式来发布作品，尝试更多新颖的传播形式。

也有人会问，这些事，你说了又有什么用，会有什么改变？

其实，我们也不认为一篇文章或一个作品可以推动出大改变，改变是需要很多不同的人、不同的群体联结起来，作长时间推动才可能发生的。

这个过程中，需要让更多人看到问题的存在，需要对议题进行再现、梳理、分析等等，这些便是我们在做的工作。

奥威尔在《我为什么写作》写到，驱使写作的其中一点动力是：“**历史方面的冲动。希望还原事物的本来面目，找出真正的事实把它们记录起来供后代使用。**”

也许纪录无法改变现状，但至少为历史留下了一个“今天的存照”。

**想实现这个目标，我们则需要接近更多的群体和现场，深入到议题中去。**这是一项费时费力，更费钱的工作，我想邀请各位读者，来帮我们实现这个构想：

![](/img/xu4.webp)

“纪录时代缺失的声音”是我们推出的一个项目，目前正在腾讯公益上筹款：

### **和大家介绍一下我们的这个项目：**

该项目以产出深度、扎实的纪录性作品为主，形式包括但不限于文字、摄像和视频。我们会基于此项目组建一个编辑和顾问团队，该团队会负责选题、内容把关和纪录者对接，同时也会支持和陪伴纪录者的创作和个人成长。

项目选题的核心议题包括：社区发展、教育、环境、流动人口、性/别、残障。

#### 我们过去做过什么？

**回访民间公益行动者**

2015年，我们回访了《一切从改变自己开始》书中的公益人，该书是自由作家、公益人寇延丁的作品，书中内容大多是在2005年前后写成，**为民间公益提供一个丰富多彩的纪录**。

在回访时，他们有的已经离开原本的领域，有的依然在公益行业中耕耘，他们的故事和思考历程，也反映出中国民间公益发展的变化，为后来者提供经验。

这是系列中的一篇：[翟明磊：十年，欣然在野](http://ngocn.blog.caixin.com/archives/135578)

**孩子的世界观，关注儿童教育**

2015年9月是抗战胜利70周年，满屏都是对胜利的庆祝，但现实中我们的战争历史教育其实是片面甚至缺失的。我们和独立影像工作者合作，记录孩子们对战争的印象和想法。

我们试图通过这个简短的视频，引起大家思考儿童教育的议题：

[又到阅兵时，来听听孩子眼中的战争是怎样的？](https://www.wxwenku.com/d/108828081)

2017年3月，一套性教育教材引发社会热议，然而事实上我们对性教育知之甚少，性教育的推行往往只侧重于青春期卫生教育、防性侵教育，而全面性教育距离我们还有很远。我们访谈了一线性教育工作者、社区公益人、学者、家长和高中生，梳理了性教育应该怎么做：

[猥亵儿童事件频发，大家都开始重视性教育了，但怎么教才好？](https://www.wxwenku.com/d/108828095)

**到现场，纪录当下**

2015年底，深圳发生滑坡事故，根据国务院调查结果，该事故是受纳场渣土堆填体的滑动，不是山体滑坡，不属于自然地质灾害，是生产安全事故。我们纪录了三个受害母亲的故事，她们的家和孩子，都被掩埋在土堆里：

[盛世深圳：埋在渣土堆下的中国宝宝](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwihxrWkw5jsAhWHwJQKHRDgAAoQFjAAegQIAhAC&url=http%3A%2F%2Fngocn.blog.caixin.com%2Farchives%2F140652&usg=AOvVaw3RidLmcttJBxAKZcSifJ3l)

2016年，常州外国语学校学生疑似因周边化工工厂残留的污染物而导致身体不适，此事引起了社会广泛关注，事实上化工园污染的问题在江苏一带非常严重。

我们深入到苏北最大的化工园区，纪录了当地村民、工厂工人的生活故事和当地的污染情况：

我们是如何用十几年时间，去毁掉一个苏北水乡？

**行动日志，梳理我们的经验**

2017年，我们推出“咸鱼打挺”系列，这是一份社会行动备忘录，选题会聚焦在针对某一公共议题而发起的行动，例如“用光头拦下光亮工程”、“社区保育行动”。

文章内容包括行动事件梳理、时间轴还原、核心参与者Q&A、经验工具包，旨在能为事件作一个完整的备忘，同时为后来者提供参考借鉴。

[那些远去的社会行动 | 公民社会在广州：先有鸡还是先有蛋？](http://www.naseng.com/n/58142/)

理想主义还在路上

支持我们，一起出发