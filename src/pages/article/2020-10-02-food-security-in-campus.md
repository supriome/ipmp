---
templateKey: article
title: 37起校园食材问题事件录
slug: food-security-in-campus
date: 2019-03-19T02:59:36.253Z
featuredpost: false
featuredimage: /img/food1.webp
trending: 1
isbrief: false
tags:
  - 食品安全
---
**资料整理 | 捞面 小田 阿七 扶雨**

**制图 | 阿七**



近日，有家长举报成都七中实验学校使用变质食材。今日下午，温江区市场监管局通报，**首批取样检测合格，样本均在保质期内，但无公布检测数据**。之前，学校发公告称，学校食堂相关责任人被停职调查，停止与原食堂承包商合作

![](https://assets.matters.news/embed/4eda8527-f71c-4db9-ae00-344d7f79257f/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87-20190319092934-jpg)

。

NGOCN搜索公开报道发现，在近3年内，被媒体报道的食堂食材问题事件共37起。其中，由家长或学生曝光的多达17起。另外，当中仅有5起确认属食堂承包方责任。可以看出校园餐问题关键不在于是否进行外包。详情如下图：

![](https://assets.matters.news/embed/f8441479-eae5-47b5-b5aa-8a29ff7c2df9/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87-20190319092844-jpg)

![](https://assets.matters.news/embed/67825642-d631-420d-95a8-b8f4858ea6f7/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87-20190319092922-jpg)

![](https://assets.matters.news/embed/ab64c2f0-d290-4964-854e-ec4f27719f44/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87-20190319093237-jpg)





![](https://assets.matters.news/embed/0060330d-0cca-4dc3-a407-e43f303665e3/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87-20190319092943-jpg)

![](https://assets.matters.news/embed/7e37ae7b-cb8b-4406-a6ba-41daa8620288/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87-20190319093916-jpg)

![](https://assets.matters.news/embed/fc1a942b-4707-4252-9592-f0102020c079/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87-20190319095236-jpg)



![](https://assets.matters.news/embed/79694edd-a8b3-4161-bc34-bebdce977e62/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87-20190319095052-jpg)